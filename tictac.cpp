
/*Maria Arranz*/

#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <string>
#include <climits>
using namespace std;

enum tipos {X,O,c};
using matriz = vector<vector<tipos>>;

using juego = struct{
    matriz tablero;
    int huecos;
    vector<bool> vacio;
    bool turno;
};


bool termina (juego EJ){
    if (EJ.huecos == 0){
        return true;
    }
    else{
        bool encontrado = false;
        for (int i = 0; i < 3 && !encontrado; ++i){
            if ((EJ.tablero[i][0] == EJ.tablero[i][1])&&
                (EJ.tablero[i][1] == EJ.tablero[i][2])&&
                (EJ.tablero[i][0] != c))
                encontrado = true;
        }
        for (int j = 0; j < 3 && !encontrado; ++j){
            if ((EJ.tablero[0][j] == EJ.tablero[1][j])&&
                (EJ.tablero[1][j] == EJ.tablero[2][j])&&
                (EJ.tablero[0][j] != c))
                encontrado = true;
        }
        if (!encontrado && (EJ.tablero[0][0] == EJ.tablero[1][1]) &&
            (EJ.tablero[1][1] == EJ.tablero[2][2]) && (EJ.tablero[0][0] != c))
            encontrado = true;
        if (!encontrado && (EJ.tablero[2][0] == EJ.tablero[1][1]) &&
            (EJ.tablero[1][1] == EJ.tablero[0][2]) && (EJ.tablero[1][1] != c))
            encontrado = true;
        return encontrado;
    }
}

int heuristica(juego EJ){
    for (int i = 0; i < 3; ++i){
        if ((EJ.tablero[i][0] == EJ.tablero[i][1])&&
            (EJ.tablero[i][1] == EJ.tablero[i][2])){
            if (EJ.tablero[i][0] == X) return 1;
            else if (EJ.tablero[i][0] == O) return -1;
        }
    }
    for (int j = 0; j < 3 ; ++j){
        if ((EJ.tablero[0][j] == EJ.tablero[1][j])&&
            (EJ.tablero[1][j] == EJ.tablero[2][j])){
            if (EJ.tablero[0][j] == X) return 1;
            else if (EJ.tablero[0][j] == O) return -1;
        }
    }
    if ((EJ.tablero[0][0] == EJ.tablero[1][1]) &&
        (EJ.tablero[1][1] == EJ.tablero[2][2])){
        if (EJ.tablero[0][0] == X) return 1;
        else if (EJ.tablero[0][0] == O) return -1;
    }
    if ((EJ.tablero[2][0] == EJ.tablero[1][1]) &&
        (EJ.tablero[1][1] == EJ.tablero[0][2])){
        if (EJ.tablero[1][1] == X) return 1;
        else if (EJ.tablero[1][1] == O) return -1;
    }
    
    return 0;
}
juego aplica (juego EJ, int c){
    if (EJ.turno){
        EJ.turno = false;
        int j = c % 3;
        int i = (c - j)/3;
        EJ.tablero[i][j] = X;
        --EJ.huecos;
        EJ.vacio[c] = false;
    }
    else{
        EJ.turno = true;
        int j = c % 3;
        int i = (c - j)/3;
        EJ.tablero[i][j] = O;
        --EJ.huecos;
        EJ.vacio[c] = false;
    }
    return EJ;
}

int valoraMin(juego EJ, int n,int alpha, int beta);
int valoraMax(juego EJ, int n,int alpha, int beta);

int valoraMax(juego EJ, int n, int alpha, int beta){
    if (termina(EJ) || n == 0) return heuristica (EJ);
    else{
        for (int i = 0; i < 9; ++i){
            if (EJ.vacio[i]){
                alpha = max(valoraMin(aplica(EJ,i), n-1,alpha,beta), alpha);
                if (alpha >= beta){
                    return alpha;
                }
            }
        }
        return alpha;
    }
}



int valoraMin(juego EJ, int n, int alpha, int beta){
    if (termina(EJ) || n == 0) return heuristica (EJ);
    else{
        for (int i = 0; i < 9; ++i){
            if (EJ.vacio[i]){
                beta = min(valoraMax(aplica(EJ,i), n-1,alpha,beta), beta);
                if (alpha >= beta){
                    return beta;
                }
            }
        }
        return beta;
    }
}

int juega (juego EJ){
    int jugada= -1;
    int mejorV = INT_MIN;
    for (int i = 0; i < 9; ++i){
        if (EJ.vacio[i]){
            int v = valoraMin(aplica(EJ,i), EJ.huecos-1,mejorV, INT_MAX);
            if (v > mejorV){
                mejorV = v;
                jugada = i;
            }
        }
    }
    return jugada;
}



//vamos a considerar que max marca X, y min marca O, el caracter c implica que en el momento de procesado la casilla estaba sin rellenar
int main() {
    juego Juego;
    Juego.tablero = matriz (3,vector<tipos>(3,c));
    Juego.vacio  = vector<bool> (9,true);
    Juego.huecos = 9;
    Juego.turno = true;
    for (int i = 0; i < 3; ++i){
        string linea;
        getline(cin, linea);
        stringstream ss;
        ss  << linea;
        for (int j = 0; j < 3;++j){
            char car;
            ss.get(car);
            if (car == 'c'){
                Juego.tablero[i][j] = c;
            }
            else if (car == 'X'){
                Juego.tablero[i][j] = X;
                --Juego.huecos;
                Juego.vacio[i*3+j] = false;
            }
            else if (car == 'O'){
                Juego.tablero[i][j] = O;
                --Juego.huecos;
                Juego.vacio[i*3+j] = false;
            }
        }
    }
    int c = juega(Juego);
    int j = c % 3;
    int i = (c - j)/3;
    cout << "El jugador debe poner una cruz en la casilla " << j +1 << " de la fila " << i+1 << '\n';
    return 0;
}


